function detectIE() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}

function fitVideoInWindow() {
    var windowH = $(window).height();

    var overlay = $('.overlay-text');
    var fixheight = windowH - overlay.position().top;
    //overlay.height(fixheight );

    var width = $(window).width();
    
    if(detectIE()) {
        windowH = width * 0.5325;
    }
    $('.branding').height(windowH);
    var navHeight = $('nav').height();
    var h = ((windowH - overlay.height()) / 2) - overlay.position().top;;
    overlay.css('padding-top', h + 'px')

    if (!Modernizr.video) {
        $('body').addClass('no-video-support');
    }
    if($(document).width() < 767) {
        $('body').addClass('no-video-mobile');
        $('#intro-video').remove();
    }
    else{
        document.getElementById('intro-video').playbackRate = 0.5;
        document.getElementById('intro-video').play();
    }
}
$(window).resize(function () {
    fitVideoInWindow();
})
$(document).ready(function () {
    fitVideoInWindow();

    $('.indicator').click(function () {
        var tabActive = $('.tab-content .active');
        var tabButton = $('a[href="#' + tabActive.attr('id') + '"]');

        if ($(this).hasClass('left')) {
            tabButton = tabButton.parent().prev();
            if (tabButton.length == 0) {
                tabButton = $('.nav-tabs .nav-item:last');
            }
        }
        else {
            tabButton = tabButton.parent().next();
            if (tabButton.length == 0) {
                tabButton = $('.nav-tabs .nav-item:first');
            }
        }
        var button = tabButton.children(0);
        if (button) {
            button.trigger('click');
        }
        return false;
    })

    $('#tab-nav a').click(function () {

    })

    $('.scroll-to').click(function () {
        var data = $(this).data();
        data.scrolltotarget = data.scrolltotarget || '.tab-content';
        data.scrolltooffset = data.scrolltooffset || -100

        var container = $('body');
        var scrollTo = $(data.scrolltotarget);

        // Or you can animate the scrolling:
        container.animate({
            scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop() + data.scrolltooffset
        }, 1200);

    });
})

window.sr = ScrollReveal({
    reset: true,
    duration: 1000,
    easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',
});
sr.reveal('.revealable-section', { duration: 1000 });